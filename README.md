# README #

This repository is for API for integration with Share File.


### Pre-requisite software ###

1.	Node (latest version / 4.2.6)
2.	Git
3.	Share File account

## How do I get development environment set up? ###
The setup includes below steps: 

* Getting the code base 
* Installation of node dependencies
* Share File configuration
* Starting Node server


### Who do I talk to? ###
* Repo owner. Drop an email to Pavan Deshmukh <pavande@cybage.com>
* Anup Hadke <anuph@cybage.com>